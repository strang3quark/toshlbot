from datetime import datetime

from toshl.account import Account
from toshl.category import Category
from toshl.client import ToshlClient
from toshl.entry import Entry
from toshl.tag import Tag


class Bot(object):
    def __init__(self, client: ToshlClient):
        self.client = client

    def get_first_account(self):
        account = Account(self.client)
        accounts = account.list()

        if not accounts or len(accounts) == 0:
            raise Exception("Could not find an account")

        return accounts[0]

    def create_entry(self, amount: float, category: str = '', tags: [str] = None, date=None, account=None):
        entry = Entry(self.client)

        if not date:
            date = datetime.now()

        if not account:
            account = self.get_first_account()

        tag_ids = []
        if tags:
            tag_client = Tag(self.client)
            for tag_name in tags:
                tag_id = tag_client.search(tag_name)
                if tag_id:
                    tag_ids.append(tag_id)

        payload = {
            'amount': amount,
            'date': date.strftime("%Y-%m-%d"),
            'account': account["id"],
            'category': Category(self.client).search(category),
            'tags': tag_ids,
            'currency': {
                'code': 'EUR'
            }
        }

        print(payload)

        return entry.create(payload)
