# TOSHL BOT

This utility allows the creation of entries in toshl.

## Setup

 - Install dependencies
         
         pip install -r requirements.txt
   
 - Export the TOSHL_TOKEN variable, you can get the token [here](https://developer.toshl.com/apps/). 
         
         export TOSHL_TOKEN="xxxxx"
   
 - Use the script to create entries, ex:
   
         python toshlbot.py entry insert -a -7 -c MyCategory -t tag1,tag2 
