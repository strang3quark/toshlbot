class Tag(object):
    def __init__(self, client):
        self.client = client

    def list(self):
        response = self.client._make_request('/tags')
        response = response.json()
        return self.client._list_response(response)

    def search(self, tag_name):
        categories = self.list()
        for c in categories:
            if c['name'] == tag_name:
                return c['id']
