import argparse
from bot.bot import Bot
from toshl.client import ToshlClient

client = ToshlClient()


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("item", help="the item you want to use [entry / etc...]")

    entry_group = parser.add_argument_group(title="Entry")
    entry_group.add_argument("action", help="the action to perform [insert]")
    entry_group_insert_group = parser.add_argument_group(title="Entry insert")
    entry_group_insert_group.add_argument("-a", help="the transaction amount, negative for expense, positive for "
                                                     "income")
    entry_group_insert_group.add_argument("-c", help="the category name")
    entry_group_insert_group.add_argument("-t", help="the tag names, comma separated")

    return parser.parse_args()


def parse(args):
    bot = Bot(client)
    if args.item == "entry":
        if args.action == "insert":
            if args.t:
                tags = args.t.split(",")
            else:
                tags = None

            bot.create_entry(args.a, args.c, tags)
        else:
            print("unknown entry action \"{}\"".format(args.item))
    else:
        print("unknown item \"{}\"".format(args.item))


if __name__ == '__main__':
    parse(argparser())
